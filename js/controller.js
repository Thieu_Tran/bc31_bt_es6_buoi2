export let glassInfor = (item, idRender) => {
  let status = "";
  if (Math.floor(Math.random() * 2) == 0) {
    status = "Out of stock";
  } else {
    status = "Stocking";
  }

  let renderInfor = (document.querySelector(idRender).innerHTML = `<Div>
                    <p>${item.name} - ${item.brand} (${item.color})
                    </p>

                    <button class="btn btn-danger">${item.price}$</button>

                    <span class="ml-2 text-success ">${status}
                    </span>

                    <p class="pt-2">${item.description}</p>

                </Div>`);
  return renderInfor;
};
