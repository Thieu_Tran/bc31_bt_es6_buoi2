let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

class GlassItem {
  constructor(
    _id,
    _src,
    _virtualImg,
    _brand,
    _name,
    _color,
    _price,
    _description
  ) {
    this.id = _id;
    this.src = _src;
    this.virtualImg = _virtualImg;
    this.brand = _brand;
    this.name = _name;
    this.color = _color;
    this.price = _price;
    this.description = _description;
  }
}
import {glassInfor} from "./controller.js"

let renderImg = () => {
  // Tạo img cho avatar
  const imageTest = document.createElement("img");
  imageTest.id = "imgTest";
  imageTest.src = "";
  imageTest.style.display = "none";
  document.querySelector("#avatar").appendChild(imageTest);

  for (let i = 0; i < dataGlasses.length; i++) {
    const image = document.createElement("img");
    let shortcut = dataGlasses[i];
    let CurrentItem = new GlassItem(
      shortcut.id,
      shortcut.src,
      shortcut.virtualImg,
      shortcut.brand,
      shortcut.name,
      shortcut.color,
      shortcut.price,
      shortcut.description
    );
    // Thêm src cho thẻ img
    image.src = CurrentItem.src;
    image.className = "col-4 w-100 vglasses__items";
    // Event khi click vào img
    image.onclick = () => {
      imageTest.src = shortcut.virtualImg;
      imageTest.style.display = "block";
      document.querySelector(".vglasses__info").style.display = "block"
      glassInfor(CurrentItem,".vglasses__info");
    };
    document.querySelector("#vglassesList").appendChild(image);
  }
};
renderImg();
// Before/After function click 
let removeGlasses = (isValue) => {
  if (isValue == true) {
    document.querySelector("#imgTest").style.display = "block";
    document.querySelector(".vglasses__info").style.display = "block"
  } else {
    document.querySelector("#imgTest").style.display = "none";
    document.querySelector(".vglasses__info").style.display = "none"
  }
};
window.removeGlasses = removeGlasses;